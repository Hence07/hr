# Phisherman Project Email Templates

## mass/
These email tamplates are less refined/authentic. The templates consist of
simple HTML with little or no adornment. There has been purposeful effort
to use poor spelling and grammar and other tip offs that the email in question
is a scam. These templates will generally be used for mass email campaigns
to verify if recepients have basic knowledge of spotting phishing attempts.

## targeted/
Contains email templates that are more refined/authentic. The templates contain
highly compressed images (https://tinypng.com/), inline CSS and HTML tables.
There has also been a concentrated effort to provide correct spelling 
and grammar whenever possible. These will generally be used for very specific 
targeting purposes (individuals or small groups)



